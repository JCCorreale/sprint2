package itunibo.jcc.planner.model;

enum class Location {
	HOME,
	FRIDGE,
	DISHWASHER,
	PANTRY,
	TABLE
}