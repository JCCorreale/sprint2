package itunibo.jcc.planner.model

enum class Item {
	BUTLER,
	FOOD,
	DISHES
}